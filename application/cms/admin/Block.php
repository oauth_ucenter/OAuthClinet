<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/12/8
 * 功能说明:
 */

namespace app\cms\admin;


use app\admin\controller\Admin;
use app\common\builder\ZBuilder;

class Block extends Admin
{
    public function index()
    {
        // 自定义按钮
        $btnMove = [
            'class' => 'btn btn-xs btn-default js-move-column',
            'icon' => 'fa fa-fw fa-arrow-circle-right',
            'title' => '移动栏目',
            'href' => url('abc', ['id' => '__id__'])
        ];


        return ZBuilder::make('table')
            ->setSearch(['name' => '栏目名称'])// 设置搜索框
            ->addColumns([ // 批量添加数据列
                ['id', 'ID'],

                ['status', '状态', 'switch'],
                ['right_button', '操作', 'btn']
            ])

            ->addTopButtons('add,enable,disable')// 批量添加顶部按钮

            ->addRightButton('custom', $btnMove, ['area' => ['830px', '95%'], 'title' => '<i class="fa fa-user"></i>组件配置'])
            ->setRowList([
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
                ['id' => 4],
                ['id' => 5],
                ['id' => 5],
            ])// 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function setblock($id = null)
    {
        if ($id !== null) {
            dump($id);
        }
    }
}