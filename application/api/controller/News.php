<?php

namespace app\api\controller;

use think\Controller;
use think\Db;
use app\cms\model\Column as ColumnModel;
use app\cms\model\Document as DocumentModel;

class News extends Controller {
    
    public function all($menu_name = 'main_nav', $menu_id = 0){
        
        $data = self::getNavData()[ $menu_name ];
        $menu_data = \util\Tree::toLayer($data, $menu_id);
        $Thisdata = null;
        foreach($data as $datum) {
            if($datum[ 'id' ] == $menu_id) {
                $Thisdata = $datum;
            }
        }
        
        return ['data' => $menu_data, 'This' => $Thisdata];
    }
    
    static public function getNavData(){
        
        $list_nav = Db::name('cms_nav')->where('status', 1)->column('id,tag');
        $NavData = [];
        foreach($list_nav as $id => $tag) {
            $data_list = Db::view('cms_menu', true)->view('cms_column', ['name' => 'column_name'], 'cms_menu.column=cms_column.id', 'left')->view('cms_page', ['title' => 'page_title'], 'cms_menu.page=cms_page.id', 'left')->where('cms_menu.nid', $id)->where('cms_menu.status', 1)->order('cms_menu.sort,cms_menu.pid,cms_menu.id')->select();
            
            foreach($data_list as &$item) {
                if($item[ 'type' ] == 0) { // 栏目链接
                    $item[ 'title' ] = $item[ 'column_name' ];
                    $item[ 'url' ] = url('cms/column/index', ['id' => $item[ 'column' ]]);
                }elseif($item[ 'type' ] == 1) { // 单页链接
                    $item[ 'title' ] = $item[ 'page_title' ];
                    $item[ 'url' ] = url('cms/page/detail', ['id' => $item[ 'page' ]]);
                }else {
                    if($item[ 'url' ] != '#' && substr($item[ 'url' ], 0, 4) != 'http') {
                        $item[ 'url' ] = url($item[ 'url' ]);
                    }
                }
            }
            $NavData[ $tag ] = $data_list;
        }
        
        return $NavData;
    }
    
    public function menu(){
        
        return $this->getNavTree('main_nav', $menu_id = 7);
    }
    
    
    public function getNavTree($menu_name = 'main_nav', $menu_id = 0){
        
        $data = \app\cms\model\Column::getNavData()[ $menu_name ];
        $menu_data = \util\Tree::toLayer($data, $menu_id);
        $Thisdata = null;
        foreach($data as $datum) {
            if($datum[ 'id' ] == $menu_id) {
                $Thisdata = $datum;
            }
        }
        
        return ['data' => $menu_data, 'This' => $Thisdata];
    }
    
    public function lists($id = null){
        
        if($id === null)
            $this->error('缺少参数');
        $map = [
            'status' => 1,
            'id' => $id
        ];
        
        $column = Db::name('cms_column')->where($map)->find();
        if(!$column)
            $this->error('该栏目不存在');
        
        $model = Db::name('cms_model')->where('id', $column[ 'model' ])->find();
        
        if($model[ 'type' ] == 2) {
            $cid_all = ColumnModel::getChildsId($id);
            $cid_all[] = (int)$id;
            
            $map = [
                $model[ 'table' ] . '.trash' => 0,
                $model[ 'table' ] . '.status' => 1,
                $model[ 'table' ] . '.cid' => ['in', $cid_all]
            ];
            
            $data_list = Db::view($model[ 'table' ], true)->view('admin_user', 'username', $model[ 'table' ] . '.uid=admin_user.id', 'left')->where($map)->order('create_time desc')->paginate(config('list_rows'));
            $this->assign('model', $column[ 'model' ]);
        }else {
            $cid_all = ColumnModel::getChildsId($id);
            $cid_all[] = (int)$id;
            
            $map = [
                'cms_document.trash' => 0,
                'cms_document.status' => 1,
                'cms_document.cid' => ['in', $cid_all]
            ];
            
            $data_list = Db::view('cms_document', true)->view('admin_user', 'username', 'cms_document.uid=admin_user.id', 'left')->view($model[ 'table' ], '*', 'cms_document.id=' . $model[ 'table' ] . '.aid', 'left')->where($map)->order('create_time desc')->paginate(config('list_rows'));
            $this->assign('model', '');
        }
        
        
        return [
            'lists' => $data_list,
            'pages' => $data_list->render(),
            'column_info' => $column
        ];
    }
    
    
    /**
     * 文档详情页
     * @param null $id 文档id
     * @param string $model 独立模型id
     *
     * @return mixed
     */
    public function detail($id = null, $model = ''){
        
        if($id === null)
            $this->error('缺少参数');
        
        if($model != '') {
            $table = get_model_table($model);
            $map = [
                $table . '.status' => 1,
                $table . '.trash' => 0
            ];
        }else {
            $map = [
                'cms_document.status' => 1,
                'cms_document.trash' => 0
            ];
        }
        
        $info = DocumentModel::getOne($id, $model, $map);
        if(isset($info[ 'tags' ])) {
            $info[ 'tags' ] = explode(',', $info[ 'tags' ]);
        }
        
        
        return $info;
    }
}
