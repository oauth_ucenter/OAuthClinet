<?php

	namespace app\index\controller;

	use util\HTTP;

	class RequestResource extends Home
	{
		public function requestResource()
		{
			$config = static::$Parameters;    //当前oauth实现的配置。

			$token = $this->request->get('token');

			// 交换授权代码用于访问令牌。
			$query = [
				'access_token' => $token
			];

			// 根据配置确定要调用的令牌端点。
			$endpoint = $config['resource_route'];
			$response = HTTP::get($endpoint.'?access_token='.$token)->toArray();
//			dump(['$response'=>$response]);die;
			$this->assign('response', $response);
			return $this->fetch('index/show_resource');


			$twig = $app['twig'];          // used to render twig templates
			$config = $app['parameters'];    // the configuration for the current oauth implementation
			$urlgen = $app['url_generator']; // generates URLs based on our routing
			$http = $app['http_client'];   // service to make HTTP requests to the oauth server

			// pull the token from the request
			$token = $app['request']->get('token');

			// determine the resource endpoint to call based on our config (do this somewhere else?)
			$apiRoute = $config['resource_route'];
			$endpoint = 0 === strpos($apiRoute, 'http') ? $apiRoute : $urlgen->generate($apiRoute, array(), true);

			// make the resource request and decode the json response
			$request = $http->get($endpoint, null, $config['http_options']);
			$request->getQuery()->set('access_token', $token);
			$response = $request->send();
			$json = json_decode((string)$response->getBody(), true);

			return $twig->render('client/show_resource.twig', array('response' => $json ? $json : $response, 'resource_uri' => $request->getUrl()));
		}

		public function Echofetch($response)
		{
			$response = (array)$response;
			$this->assign('response', $response);
//			http://localhost:8080/client/request_resource?token=bcb0ed8178928b9ae482c6ef0ff2f5bc2191ffc1
			if (isset($response['access_token'])) {
				return $this->fetch('index/show_refresh_token');
			} else {
				return $this->fetch('index/failed_token_request');
			}

		}


	}
