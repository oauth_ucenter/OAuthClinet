<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

	namespace app\index\controller;


	use OAuth2\HttpFoundationBridge\Response as BridgeResponse;
	use OAuth2\HttpFoundationBridge\Request;
	use app\common\controller\Common;

	/**
	 * 前台公共控制器
	 * @package app\index\controller
	 */
	class Home extends Common
	{
		static public $OAuthServerUrl    = '/index/index/collback';
		static public $OAuthServerDomain = 'http://server.oauth.com';
		static public $Parameters        = [
			"client_id"        => "demoapp",
			"client_secret"    => "demopass",
			"token_route"      => "http://server.oauth.com/index/token/token",//令牌
			"authorize_route"  => "authorize",
			"resource_route"   => "http://server.oauth.com/index/resource",//资源
			"resource_params"  => [],
			"user_credentials" => ["demouser", "testpass"],
			"http_options"     => ["exceptions" => false],
		];


		/**
		 * 初始化方法
		 *
		 */
		protected function _initialize()
		{
			// 系统开关
			if (!config('web_site_status')) {
				$this->error('站点已经关闭，请稍后访问~');
			}

			$this->oauth_response = new BridgeResponse();

			$this->oauth_request = Request::createFromGlobals();
		}

		/**
		 * 创建静态页面
		 * @access protected
		 * @htmlfile 生成的静态文件名称
		 * @htmlpath 生成的静态文件路径
		 * @param string $templateFile 指定要调用的模板文件
		 * 默认为空 由系统自动定位模板文件
		 * @return string
		 *
		 */
		protected function buildHtml($htmlfile = '', $htmlpath = ROOT_PATH . '/runtime/', $templateFile = '')
		{
			$content = $this->fetch($templateFile);
			$htmlpath = !empty($htmlpath) ? $htmlpath : './appTemplate/';
			$htmlfile = $htmlpath . $htmlfile . '.' . config('url_html_suffix');

			$File = new \think\template\driver\File();
			$File->write($htmlfile, $content);
			return $content;
		}

		/**
		 * combineURL
		 * 拼接url
		 * @param string $baseURL 基于的url
		 * @param array $keysArr 参数列表数组
		 * @return string           返回拼接的url
		 */
		static public function combineURL($baseURL, $keysArr)
		{
			if (!empty($keysArr) || $keysArr !== '') {

				$combined = $baseURL . "?";
				$valueArr = array();

				foreach ($keysArr as $key => $val) {
					$valueArr[] = "$key=$val";
				}

				$keyStr = implode("&", $valueArr);
				$combined .= ($keyStr);

				return $combined;
			} else {
				return $baseURL;
			}
		}

		/**
		 * 判断是否为PHP命令行执行的服务器
		 */
		public function testForBuiltInWebServer()
		{
			if (php_sapi_name() == 'cli-server') {
				$message = 'As PHP\'s built-in web-server does not allow for concurrent requests, this will result in deadlock.';
				$message .= "<br /><br />";
				$message .= 'You must configure a virtualhost via Apache or another web server to continue.  Sorry!';
				exit($message);
			}
		}
	}
