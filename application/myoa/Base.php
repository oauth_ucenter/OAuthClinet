<?php

namespace app\wechat;

use EasyWeChat\Foundation\Application;

trait Base
{
    protected $options = [

    ];

    protected $app;

    public function _initialize()
    {
        parent::_initialize();
        $config = module_config('myoa');
        $this->options = array_merge($this->options, $config);
        $this->app = new Application($this->options);
    }

}