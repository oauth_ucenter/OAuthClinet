<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/23
 * 功能说明:
 */

namespace app\myoa\model;


class ContractModel extends CommonModel
{
    // 设置当前模型对应的完整数据表名称
    protected $table = '__MYOA_CONTRACT__';

    // 自动写入时间戳
    protected $autoWriteTimestamp = true;


    public static function getList($map = [])
    {
        $result = cache('myoa_client');
        if (!$result) {
            $result = self::view('myoa_contract', true)->where($map)->paginate();

            // 非开发模式，缓存数据
            if (config('develop_mode') == 0) {
                cache('myoa_client', $result);
            }
        }
        return $result;
    }
}