<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/22
 * 功能说明:
 */

namespace app\myoa\model;


class ObjectModel extends CommonModel
{
    // 设置当前模型对应的完整数据表名称
    protected $table = '__MYOA_OBJECT__';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;


}