<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/23
 * 功能说明:
 */

namespace app\myoa\model;


class TypeModel extends CommonModel
{
    // 设置当前模型对应的完整数据表名称
    protected $table = '__MYOA_TYPE__';

    const STATUS = [
        1 => '项目类型',
        2 => '合同类型',
    ];

    static public function getStart($map)
    {
        return self::where($map)->column('id,name');
    }

}