<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/23
 * 功能说明:
 */

namespace app\myoa\model;

use think\Model as ThinkModel;

class CommonModel extends ThinkModel
{
    static public function getList($map = [])
    {
        $result = cache(__CLASS__);
        if (!$result) {
            $result = self::view('myoa_contract', true)->where($map)->paginate();
            // 1非开发模式，缓存数据
            if (config('develop_mode') == 0) {
                cache(__CLASS__, $result);
            }
        }
        return $result;
    }

    static public function getIDAndName($map = [], $field = 'id,name')
    {
        $result = cache(__CLASS__);
        if (!$result) {
            $result = static::where($map)->column($field);

            // 2非开发模式，缓存数据
            if (config('develop_mode') == 0) {
                cache(__CLASS__, $result);
            }
        }
        return $result;
    }

    static public function getNameById($map = [])
    {
        $result = cache(__CLASS__);
        if (!$result) {
            $result = static::where($map)->column('id,name');

            // 3非开发模式，缓存数据
            if (config('develop_mode') == 0) {
                cache(__CLASS__, $result);
            }
        }
        return $result;
    }
}