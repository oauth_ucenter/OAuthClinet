<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/22
 * 功能说明:
 */

namespace app\myoa\admin;

use app\admin\controller\Admin;
use app\myoa\model\ClientModel;
use app\myoa\model\ContractModel;
use app\myoa\model\ObjectModel;
use app\common\builder\ZBuilder;
use app\myoa\model\StatusModel;
use app\myoa\model\TypeModel;

class Object extends Admin
{
    public function index()
    {
        cookie('__forward__', $_SERVER['REQUEST_URI']);
        // 查询
        $map = $this->getMap();
        // 排序
        $order = $this->getOrder('update_time desc');
        // 数据列表
        $data_list = ObjectModel::where($map)->order($order)->column(true);

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setSearch(['name' => '客户名称', 'ink_name' => '联系人'])// 设置搜索框
            ->addColumns([ // 批量添加数据列
                ['id', 'ID'],
                ['name', '项目名称'],
                ['leader', '项目负责人'],
                ['client_id', '客户名称', ClientModel::getIDAndName()],
                ['object_type', '项目类型', TypeModel::getStart(['type' => 1])],
                ['creat_object_date', '项目开始时间', 'callback', function ($value) {
                    return explode(' ', $value)[0];
                }],
                ['contract_id', '合同编号', 'callback', function ($value) {
                    return ContractModel::getIDAndName([], 'id,number')[$value];
                }],
                ['status', '项目状态', StatusModel::getStart(['type' => 2])],
                ['remark', '备注'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete')// 批量添加顶部按钮
            ->addRightButtons(['edit', 'delete'])// 批量添加右侧按钮
            ->addTimeFilter('create_time')// 添加时间段筛选
            ->setRowList($data_list)// 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function add($pid = 0)
    {
        // 保存数据
        if ($this->request->isPost()) {
            // 表单数据
            $data = $this->request->post();

//            // 验证
//            $result = $this->validate($data, 'Column');
//            if (true !== $result) $this->error($result);

            if ($column = ObjectModel::create($data)) {
                cache('myoa_Object', null);
                // 记录行为
                action_log('Object_add', 'myoa_Object', $column['id'], UID, $data['name']);
                $this->success('新增成功', 'index');
            } else {
                $this->error('新增失败');
            }
        }
//                dump(ContractModel::getIDAndName([],'id,number as name'));
//                dump(ClientModel::getIDAndName());die;
        // 显示添加页面
        return ZBuilder::make('form')
            ->addFormItems([
                ['text', 'name', '项目名称', '<span class="text-danger">必填</span>'],
                ['text', 'leader', '项目负责人', '<span class="text-danger">必填</span>'],
                ['select', 'client_id', '客户名称', '<span class="text-danger">必填</span>', ClientModel::getIDAndName()],
                ['select', 'object_type', '项目类型', '<span class="text-danger">必填</span>', TypeModel::getStart(['type' => 1])],
                ['date', 'creat_object_date', '项目开始日期', '<span class="text-danger">必选</span>'],
                ['select', 'contract_id', '合同编号', '', ContractModel::getIDAndName([], 'id,number')],
                ['select', 'status', '状态', '', StatusModel::getStart(['type' => 2]), 1],
                ['textarea', 'remark', '备注'],
            ])
            ->layout([
                'name' => 3, 'leader' => 3, 'client_id' => 3,
                'object_type' => 3, 'creat_object_date' => 3,
                'contract_id' => 3, 'status' => 3,
            ])
            ->fetch();
    }

    public function edit($id = '')
    {
        if ($id === 0) $this->error('参数错误');

        // 保存数据
        if ($this->request->isPost()) {
            $data = $this->request->post();

            if (ObjectModel::update($data)) {
                // 记录行为
                action_log('Object_edit', 'myoa_Object', $id, UID, $data['name']);
                return $this->success('编辑成功', 'index');
            } else {
                return $this->error('编辑失败');
            }
        }

        // 获取数据
        $info = ObjectModel::get($id);
        // 显示编辑页面
        return ZBuilder::make('form')
            ->addFormItems([
                ['hidden', 'id'],
                ['static', 'name', '项目名称', '<span class="text-danger">必填</span>', '', true],
                ['text', 'leader', '项目负责人', '<span class="text-danger">必填</span>'],
                ['select', 'client_id', '客户名称', '<span class="text-danger">必填</span>', ClientModel::getIDAndName()],
                ['select', 'object_type', '项目类型', '<span class="text-danger">必填</span>', TypeModel::getStart(['type' => 1])],
                ['date', 'creat_object_date', '项目开始日期', '<span class="text-danger">必选</span>'],
                ['select', 'contract_id', '合同编号', '', ContractModel::getIDAndName([], 'id,number')],
                ['select', 'status', '状态', '', StatusModel::getStart(['type' => 2]), 1],
                ['textarea', 'remark', '备注'],
            ])
            ->setFormData($info)
            ->layout([
                'name' => 3, 'leader' => 3, 'client_id' => 3,
                'object_type' => 3, 'creat_object_date' => 3,
                'contract_id' => 3, 'status' => 3,
            ])
            ->fetch();
    }
}