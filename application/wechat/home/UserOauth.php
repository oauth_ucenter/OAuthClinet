<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/19
 * 功能说明:
 */

namespace app\wechat\home;

use EasyWeChat\Foundation\Application;

use app\index\controller\Home;
use app\wechat\model\WeChat;
use think\Session;

class UserOauth extends Home
{


    public function index()
    {

        $app = WeChat::Main(
            [
                'oauth' => [
                    'scopes' => ['snsapi_userinfo'],
                    'callback' => '/wechat/user/oauth_callback',
                ],
            ]);
        $oauth = $app->oauth;
        if (empty(Session::get('wechat_user'))) {
            Session::set('target_url', '/wechat/user/index');
            return $oauth->redirect();
        }
    }

    public function oauth_callback()
    {
        $app = WeChat::Main();
        $oauth = $app->oauth;
        $user = $oauth->user();
        Session::set('wechat_user', $user);
        $targetUrl = empty(Session::get('target_url')) ? '/wechat/user/getuser' : Session::get('target_url');
        header('location:' . $targetUrl); // 跳转到 user/profile
    }


    public function getuser()
    {
        $user = Session::get('wechat_user');
        //将用户的基本信息保存在数据库中，然后提供下次进行使用
        print_r($user);
    }


    public function alluser(){
        $userService = WeChat::Main()->user->lists($nextOpenId = null);

        dump($userService);
    }


}