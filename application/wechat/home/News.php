<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/20
 * 功能说明:
 */

namespace app\wechat\home;

use EasyWeChat\Message\News as WeNews;
use app\index\controller\Home;

class News extends Home
{

    /**
     * - title 标题
     * - description 描述
     * - image 图片链接
     * - url 链接 URL
     */
    public function index()
    {

        $news = new WeNews([
            'title' => '这里是文章标题',
            'description' => '这里是文章描述',
            'url' => 'www.baidu.com',
            'image' => 'http://mmbiz.qpic.cn/mmbiz_jpg/Ux4k3kE9GA60fvicX8OlMNofUuIJP0VicNXmwwNibknd5VrTRMia8sekttzrnoFYpWZlic5TbQtyLIYZAuOIjxias6Vw/0',
            // ...
        ]);
// or
        $news = new News();
        $news->title = 'EasyWeChat';
        $news->description = '微信 SDK ...';
    }
}