<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/22
 * 功能说明:
 */

namespace app\wechat\home;


use app\index\controller\Home;
use app\wechat\model\WeChat;

class User extends Home
{
    public function index()
    {
        $app = WeChat::Main();
        $userService = $app->user;
        $UserList = $userService->lists($nextOpenId = null);  // $nextOpenId 可选
        dump($UserList);
    }

}