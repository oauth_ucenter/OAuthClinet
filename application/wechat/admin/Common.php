<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/19
 * 功能说明:
 */

namespace app\wechat\admin;

use app\admin\controller\Admin;
use EasyWeChat\Foundation\Application;
use think\Request;

class Common extends Admin
{
    static $app = null;

    public function __construct(Request $request = null)
    {
        self::$app = new Application(config()['wechat_config']);
        parent::__construct($request);
    }


}