<?php


namespace app\wechat\admin;

use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\wechat\model\WeReply as WeReplyModel;
use app\wechat\model\WeMaterial as WeMaterialModel;

use EasyWeChat\Foundation\Application;
use think\Request;

/**
 *
 */
class Menu extends Admin
{
    private $app;

    public function __construct(Request $request = null)
    {
        $this->app = new Application(config()['wechat_config']);
        parent::__construct($request);
    }


    public function add()
    {


        $menu = $this->app->menu;

        $buttons = [
            [
                "type" => "click",
                "name" => "今日歌曲",
                "key" => "V1001_TODAY_MUSIC"
            ],
            [
                "name" => "菜单",
                "sub_button" => [
                    [
                        "type" => "view",
                        "name" => "搜索",
                        "url" => "http://www.soso.com/"
                    ],
                    [
                        "type" => "view",
                        "name" => "视频",
                        "url" => "http://v.qq.com/"
                    ],
                    [
                        "type" => "click",
                        "name" => "赞一下我们",
                        "key" => "V1001_GOOD"
                    ],
                ],
            ],
        ];
        echo $menu->add($buttons) ? "OK" : "NO";
    }

    public function index()
    {
        $userService = $this->app->user;
        $allUser = $userService->lists($nextOpenId = null);

        dump($allUser);
    }
}