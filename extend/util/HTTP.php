<?php
/**
 * Created by PhpStorm.
 * User: mybook-lhp
 * Date: 18/1/8
 * Time: 下午5:52
 */

namespace util;

//curl类
class HTTP
{
    static protected $json = null;
    static protected $json_data = null;

    private function __construct($method, $url, $fields = '', $userAgent = '', $httpHeaders = '', $username = '', $password = '')
    {
        $ch = self::create();
        if (false === $ch) {
            return false;
        }
        if (is_string($url) && strlen($url)) {
            $ret = curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            return false;
        }
        //是否显示头部信息
        curl_setopt($ch, CURLOPT_HEADER, false);
        //
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($username != '') {
            curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
        }
        $method = strtolower($method);
        if ('post' == $method) {
            curl_setopt($ch, CURLOPT_POST, true);
            if (is_array($fields)) {
                $sets = array();
                foreach ($fields AS $key => $val) {
                    $sets[] = $key . '=' . urlencode($val);
                }
                $fields = implode('&', $sets);
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        } else if ('put' == $method) {
            curl_setopt($ch, CURLOPT_PUT, true);
        }
        //curl_setopt($ch, CURLOPT_PROGRESS, true);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);
        //curl_setopt($ch, CURLOPT_MUTE, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);//设置curl超时秒数
		dump($userAgent);
        if (strlen($userAgent)) {
            curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        }
        if (is_array($httpHeaders)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
        }
        $ret = curl_exec($ch);
        if (curl_errno($ch)) {
            curl_close($ch);
            self::$json_data = array(curl_error($ch), curl_errno($ch));
        } else {
            curl_close($ch);
            if (!is_string($ret) || !strlen($ret)) {
                self::$json_data = false;
            }

            self::$json_data = $ret;
        }
    }

    static function post($url, $fields, $userAgent = '', $httpHeaders = '', $username = '', $password = '')
    {
        if (static::$json === null) {
            static::$json = new self('POST', $url, $fields, $userAgent, $httpHeaders, $username, $password);
        }
        return static::$json;
    }


    static function get($url, $userAgent = '', $httpHeaders = '', $username = '', $password = '')
    {
    	trace($url);
        if (static::$json === null) {
            static::$json = new self('GET', $url, '', $userAgent, $httpHeaders, $username, $password);
        }
        return static::$json;
    }


    static function create()
    {
        $ch = null;
        if (!function_exists('curl_init')) {
            return false;
        }
        $ch = curl_init();
        if (!is_resource($ch)) {
            return false;
        }
        return $ch;
    }

    public function toArray()
    {
        if (false === static::$json_data) {
            return false;
        }
        if (is_array(static::$json_data)) {
            return false;
        }
        return json_decode(json_decode(static::$json_data, true),true);
    }

    public function toString()
    {
        if (false === static::$json_data) {
            return false;
        }
        if (is_array(static::$json_data)) {
            return false;
        }

        return json_decode(json_decode(static::$json_data, true));
    }
}