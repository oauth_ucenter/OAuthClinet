<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/12/6
 * 功能说明:
 */

namespace plugins\Block\model;

use think\Model;
use util\File;

class BlockModel extends Model
{
    static public $ConfigJson = [];
    static public $StaticInfo = [];
    static public $ConfigIMG = [];

    /**
     * 获取所有Block基本信息
     */
    static public function getAllConfig()
    {

        $ConfigPath = File::getDir(dirname(dirname(__FILE__)) . '/view');

        foreach ($ConfigPath as $cPath) {
            if (strstr($cPath, 'config.json')) {
                $Info = json_decode(file_get_contents($cPath));
                self::$ConfigJson[$Info->info->identifier] = $Info;
            }


            if (strstr($cPath, '.js')) {
                self::$StaticInfo['js'][] = '/' . str_replace(ROOT_PATH, '', $cPath);
            } elseif (strstr($cPath, '.css')) {
                self::$StaticInfo['css'][] = '/' . str_replace(ROOT_PATH, '', $cPath);
            } elseif (strstr($cPath, '/img/')) {
                self::$ConfigIMG['img'] = '/' . str_replace(ROOT_PATH, '', $cPath);
            }
        }

    }

    /**
     * 获取图片绝对路径
     * @param string $img
     * @return string
     */
    static public function getImg($img = '')
    {
        return dirname(dirname(__FILE__)) . '/view/' . $img;
    }

    /**
     * 分Block处理所有图片路径
     * @return array
     */
    static public function StaticImg()
    {
        $image = [];

        foreach (array_keys(self::$ConfigJson) as $item) {

            foreach (self::$ConfigIMG as $value) {

                $image[$item][] = str_replace(ROOT_PATH, '', dirname(dirname(__FILE__)) . '/view/' . strstr($value, $item));

            }
        }

        return $image;
    }

    /**
     * 获取所有JS和CSS的绝对路径
     * @param null $type
     * @return array|mixed
     */
    static public function staticInfo($type = null)
    {
        self::getAllConfig();
        if ($type !== null && !empty(self::$StaticInfo)) {
            return self::$StaticInfo[$type];
        }
        return self::$StaticInfo;
    }

    /**
     * 文件目录查找辅助方法
     * @param $path
     * @param $data
     */
    static function searchDir($path, &$data)
    {
        if (is_dir($path)) {
            $dp = dir($path);
            while ($file = $dp->read()) {
                if ($file != '.' && $file != '..') {
                    self::searchDir($path . '/' . $file, $data);
                }
            }
            $dp->close();
        }
        if (is_file($path)) {
            $data[] = $path;
        }
    }

    /**
     * 文件目录查找辅助方法
     * @param $dir
     * @return array
     */
    static function getDir($dir)
    {
        $data = array();
        static::searchDir($dir, $data);
        return $data;
    }

}