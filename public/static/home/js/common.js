/**
 * anthor: 高旸
 * time: 2017-11-16
 */

/* Tab */
function tab(wrap, tabItem, tabCon, activeClassName, hover) {
    var activeClassName = (activeClassName == undefined) ? 'active' : activeClassName;

    $(tabItem, wrap).eq(0).addClass(activeClassName);
    $(tabCon, wrap).hide().eq(0).show();

    if (!hover || hover == undefined) {
        $(tabItem, wrap).on("click", function() {
            var index = $(this).index();
            $(tabItem, wrap).removeClass(activeClassName).eq(index).addClass(activeClassName);
            $(tabCon, wrap).hide().eq(index).show();
        });
    } else {
        $(tabItem, wrap).hover(function() {
            var index = $(this).index();
            $(tabItem, wrap).removeClass(activeClassName).eq(index).addClass(activeClassName);
            $(tabCon, wrap).hide().eq(index).show();
        });
    }
}

/* mobileAction */
var mobileAction = {
    init:function(){
        mobileAction.navBtn();
    },
    navBtn:function(){
        $("#navBtnMobile").bind("click",function(){
            if($(".nav:visible").length) {
                $(".nav").hide();
            } else {
                $(".nav").show();
            }
        });
    }
};


var scrollthis;
$(window).scroll(function() {
    if ($(window).scrollTop() > 0 && !$(".float-right-box").hasClass('show')) {
        $(".float-right-box").addClass('show');
        $(".float-right-box").stop(true,true).addClass('on');
    }
});
/* slidebar*/
$(".float-right-box").hover(function() {
    $(this).stop(true,true).removeClass('on');
}, function() {
    var _this = $(this);
    if ($(window).scrollTop() > 0) {
        _this.stop(true,true).addClass('on');
    }else{
        $(".float-right-box").removeClass('show');
    }
});

$(function() {
    mobileAction.init();
    tab(".case-list",".tab-link-item",".tab-item","active");
    tab(".case-content",".tab-link-item",".case-card","active");

    $(".return-webtop").on('click', function() {
        $("body,html").stop().animate({scrollTop: 0}, 500);
    });

});
